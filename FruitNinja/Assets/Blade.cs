﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class Blade : MonoBehaviour
{

    public GameObject BladeTrailPrefab;
    public float timeTrailDestroy = 2f;

    public float minCuttingVelocity = 0.001f;

    private bool isCutting = false;

    private Vector2 previousPosition;

    private Rigidbody2D rb;
    private Camera cam;
    private CircleCollider2D circleCollider;

    private GameObject currentBladeTrail;
    

    // Use this for initialization
    void Start ()
    {
        circleCollider = GetComponent<CircleCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        cam = Camera.main;

        circleCollider.enabled = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetMouseButtonDown(0))
        {
            StartCutting();
        }
        else if(Input.GetMouseButtonUp(0))
        {
            StopCutting();
        }

        if(isCutting)
        {
            UpdateCut();
        }
		
	}

    private void UpdateCut()
    {
        Vector2 newPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        rb.position = newPosition;

        float velocity = (newPosition - previousPosition).magnitude * Time.deltaTime;

        if(velocity > minCuttingVelocity)
        {
            circleCollider.enabled = true;
        }
        else
        {
            circleCollider.enabled = false;
        }

        previousPosition = newPosition;

    }

    private void StartCutting()
    {
        isCutting = true;
        currentBladeTrail = Instantiate(BladeTrailPrefab,transform);
        previousPosition = cam.ScreenToWorldPoint(Input.mousePosition);
        circleCollider.enabled = false;
    }

    private void StopCutting()
    {
        isCutting = false;
        currentBladeTrail.transform.SetParent(null);
        Destroy(currentBladeTrail, timeTrailDestroy);

        circleCollider.enabled = false;
    }
}
