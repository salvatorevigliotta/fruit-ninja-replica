﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitSpawner : MonoBehaviour
{
    public GameObject FruitPrefab;
    public Transform[] SpawnPoints;

    public float MinDelay = 0.1f;
    public float MaxDelay = 1f;

    public float TimeToDestroyFruit = 5f;

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(SpawnFruits());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator SpawnFruits()
    {
        while(true)
        {
            float delay = Random.Range(MinDelay, MaxDelay);
            yield return new WaitForSeconds(delay);

            int spawnIndex = Random.Range(0, SpawnPoints.Length);
            Transform spawnPoint = SpawnPoints[spawnIndex];

            GameObject spawnedFruit =  Instantiate(FruitPrefab, spawnPoint.position, spawnPoint.rotation);
            Destroy(spawnedFruit, TimeToDestroyFruit);

        }
    }
}
