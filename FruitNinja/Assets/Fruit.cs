﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Fruit : MonoBehaviour
{

    public GameObject FruitSlicedPrefab;
    public float StartForce = 15f;

    public float DestroySlicedFruitTime = 3f;

    private Rigidbody2D rb;

	// Use this for initialization
	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();

        rb.AddForce(transform.up * StartForce, ForceMode2D.Impulse);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Blade")
        {
            Vector3 direction = (col.transform.position - transform.position).normalized;
            Quaternion rotation = Quaternion.LookRotation(direction);

            GameObject slicedFruit = Instantiate(FruitSlicedPrefab, transform.position, rotation);
            Destroy(slicedFruit, DestroySlicedFruitTime);
            Destroy(gameObject);
        }
    }
}
